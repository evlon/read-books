package com.talkweb.reader.slice;

import com.talkweb.baselibrary.bar.bottom.BottomBarInfo;
import com.talkweb.baselibrary.bar.bottom.BottomNavigationBar;
import com.talkweb.baselibrary.bar.common.IBarLayout;
import com.talkweb.baselibrary.bar.component.BottomBarComponentProvider;
import com.talkweb.baselibrary.bar.component.FractionBarComponent;
import com.talkweb.baselibrary.base.BaseAbilitySlice;
import com.talkweb.baselibrary.base.BaseFractionAbilitySlice;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.book.BookFraction;
import com.talkweb.reader.circle.CircleFraction;
import com.talkweb.reader.maintab.MainFraction;
import com.talkweb.reader.mine.MineFraction;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.content.Intent;
import ohos.multimodalinput.event.KeyEvent;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends BaseFractionAbilitySlice {

    /**
     * 底部导航栏的数据
     */
    private List<BottomBarInfo<?>> bottomInfoList = new ArrayList<>();
    /**
     * 底部导航栏
     */
    private BottomNavigationBar bottomNavigationBar;
    private FractionBarComponent mFractionBarComponent;
    private int defaultColor;
    private int tintColor;


    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_ability_main;
    }

    @Override
    protected void initComponent() {
        bottomNavigationBar = findById(ResourceTable.Id_bottomNavigationBar);
        mFractionBarComponent = findById(ResourceTable.Id_fractionbarComponent);
        defaultColor = getColor(ResourceTable.Color_colorGray);
        tintColor = getColor(ResourceTable.Color_colorBlue);
        String zhuye = getString(ResourceTable.String_zhuye);
        String shujia = getString(ResourceTable.String_shujia);
        String quanzi = getString(ResourceTable.String_quanzi);
        String mine = getString(ResourceTable.String_mine);

        // 主页
        BottomBarInfo<Integer> mianInfo = new BottomBarInfo<>(zhuye,
                ResourceTable.Media_zhuye,
                ResourceTable.Media_zhuye_selected,
                defaultColor, tintColor);
        mianInfo.fraction = MainFraction.class;
        // 书架
        BottomBarInfo<Integer> bookInfo = new BottomBarInfo<>(shujia,
                ResourceTable.Media_shujia,
                ResourceTable.Media_shujia_selected,
                defaultColor, tintColor);
        bookInfo.fraction = BookFraction.class;
        // 圈子
        BottomBarInfo<Integer> circleInfo = new BottomBarInfo<>(quanzi,
                ResourceTable.Media_quanzi,
                ResourceTable.Media_quanzi_selected,
                defaultColor, tintColor);
        circleInfo.fraction = CircleFraction.class;

        // 我的
        BottomBarInfo<Integer> mineInfo = new BottomBarInfo<>(mine,
                ResourceTable.Media_gerenzhongxin,
                ResourceTable.Media_gerenzhongxin_selected,
                defaultColor, tintColor);
        mineInfo.fraction = MineFraction.class;

        bottomInfoList.add(mianInfo);
        bottomInfoList.add(bookInfo);
        bottomInfoList.add(circleInfo);
        bottomInfoList.add(mineInfo);

        // 设置底部导航栏的透明度
        bottomNavigationBar.setBarBottomAlpha(0.85f);
        // 初始化所有的条目
        bottomNavigationBar.initInfo(bottomInfoList);
        initFractionBarComponent();
        bottomNavigationBar.addBarSelectedChangeListener((index, prevInfo, nextInfo) ->
                // 显示fraction
                mFractionBarComponent.setCurrentItem(index));
        // 设置默认选中的条目，该方法一定要在最后调用
        bottomNavigationBar.defaultSelected(mianInfo);
    }

    private void initFractionBarComponent() {
        FractionManager fractionManager = getFractionManager();
        BottomBarComponentProvider provider = new BottomBarComponentProvider(bottomInfoList, fractionManager);
        mFractionBarComponent.setProvider(provider);
    }

    @Override
    protected void getData() {

    }

    private long mExitTime = 0;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEY_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                showToast(this,ResourceTable.String_exit_app);
                mExitTime = System.currentTimeMillis();
            } else {
                terminateAbility();
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onBackPressed() {

    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
