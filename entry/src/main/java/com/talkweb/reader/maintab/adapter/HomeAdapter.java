package com.talkweb.reader.maintab.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.talkweb.baselibrary.banner.ConvenientBanner;
import com.talkweb.baselibrary.component.CommonImageText;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.bookshelf.adapter.OnBookShelfAdapterClickListener;
import com.talkweb.reader.maintab.bean.HomeDesignBean;
import com.talkweb.reader.maintab.bean.SourceListContent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.List;

public class HomeAdapter extends BaseItemProvider {

    private int mHeaderCount = 1;// 头部的数量
    private int mRecommendCount = 1;//经典推荐的数量

    // 首先定义几个常量标记item的类型
    private static final int ITEM_TYPE_HEADER = 0;
    private static final int ITEM_TYPE_CONTENT = 1;
    private static final int ITEM_TYPE_RECOMMEND = 2;

    private Context context;
    private List<HomeDesignBean> listContent;
    //中间内容位置信息
    private int mContentPosition;

    //各种点击时间接口,实现在fragment中
    private OnHomeAdapterClickListener listener;
    //轮播图数据源
    private List<String> carouselImages;

    public void setListContent(List<HomeDesignBean> listContent) {
        this.listContent = listContent;
    }

    public void setCarouselImages(List<String> carouselImages) {
        this.carouselImages = carouselImages;
    }

    public void setRecommendList(List<SourceListContent> recommendList) {
        this.recommendList = recommendList;
    }

    public void setNewData(List<HomeDesignBean> listContent,List<String> carouselImages,List<SourceListContent> recommendList){
        this.listContent = listContent;
        this.carouselImages = carouselImages;
        this.recommendList = recommendList;
        notifyDataChanged();
    }

    //推荐位数据源
    private List<SourceListContent> recommendList;


    public HomeAdapter(Context context, List<HomeDesignBean> listContent
           , List<String> carouselImages, List<SourceListContent> recommendList ,
                       OnHomeAdapterClickListener listener) {
        this.context = context;
        this.listContent = listContent;
        this.listener = listener;
        this.carouselImages = carouselImages;
        this.recommendList = recommendList;
    }

    // 中间内容长度
    private int getContentItemCount() {
        return listContent.size();
    }

    // 判断当前item是否是头部（根据position来判断）
    private boolean isHeaderView(int position) {
        return mHeaderCount != 0 && position < mHeaderCount;
    }


    // 判断当前item是否为经典推荐位
    private boolean isRecommendView(int position) {
        return mRecommendCount != 0 && position == 1;
    }

    @Override
    public int getCount() {
        return listContent.size() + mHeaderCount + mRecommendCount;
    }

    @Override
    public Object getItem(int position) {
        return listContent.get(position - mHeaderCount - mRecommendCount);
    }

    @Override
    public long getItemId(int position) {
        return position - mHeaderCount - mRecommendCount;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        HeadViewHolder headViewHolder = null;
        RecommendViewHolder recommendViewHolder = null;
        ListViewHolder listViewHolder = null;
        int type = getItemComponentType(position);
        mContentPosition = position - mHeaderCount - mRecommendCount;
        if (component == null) {
            switch (type) {
                case ITEM_TYPE_HEADER:
                    component = LayoutScatter.getInstance(context).parse(
                            ResourceTable.Layout_home_layout_header, componentContainer, false);
                    headViewHolder = new HeadViewHolder();

                    headViewHolder.convenientBanner = (ConvenientBanner) component.findComponentById(ResourceTable.Id_banner);
                    headViewHolder.serch_sl = (StackLayout) component.findComponentById(ResourceTable.Id_serch_sl);
                    headViewHolder.dongman_cit = (CommonImageText) component.findComponentById(ResourceTable.Id_dongman_cit);
                    headViewHolder.mingxinpian_cit = (CommonImageText) component.findComponentById(ResourceTable.Id_mingxinpian_cit);
                    headViewHolder.shujia_cit = (CommonImageText) component.findComponentById(ResourceTable.Id_shujia_cit);
                    headViewHolder.guangchang_cit = (CommonImageText) component.findComponentById(ResourceTable.Id_guangchang_cit);

                    component.setTag(headViewHolder);
                    break;
                case ITEM_TYPE_RECOMMEND:
                    component = LayoutScatter.getInstance(context).parse(
                            ResourceTable.Layout_home_item_recommend, componentContainer, false);
                    recommendViewHolder = new RecommendViewHolder();
                    recommendViewHolder.firstLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_firstLayout);
                    recommendViewHolder.twoLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_twoLayout);
                    recommendViewHolder.threeLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_threeLayout);
                    recommendViewHolder.mp_home_recommend_firstImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_firstImage);
                    recommendViewHolder.mp_home_recommend_twoImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_twoImage);
                    recommendViewHolder.mp_home_recommend_threeImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_threeImage);
                    recommendViewHolder.mp_home_recommend_fristText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_fristText);
                    recommendViewHolder.mp_home_recommend_twoText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_twoText);
                    recommendViewHolder.mp_home_recommend_threeText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_threeText);
                    component.setTag(recommendViewHolder);
                    break;
                case ITEM_TYPE_CONTENT:
                    component = LayoutScatter.getInstance(context).parse(
                            ResourceTable.Layout_home_item_list, componentContainer, false);
                    listViewHolder = new ListViewHolder();
                    listViewHolder.cardTitleImage = (Image) component.findComponentById(ResourceTable.Id_cardTitleImage);
                    listViewHolder.cardFristImage = (Image) component.findComponentById(ResourceTable.Id_cardFristImage);
                    listViewHolder.cardTowImage = (Image) component.findComponentById(ResourceTable.Id_cardTowImage);
                    listViewHolder.cardThreeImage = (Image) component.findComponentById(ResourceTable.Id_cardThreeImage);
                    listViewHolder.cardFourImage = (Image) component.findComponentById(ResourceTable.Id_cardFourImage);
                    listViewHolder.contentChange = (Image) component.findComponentById(ResourceTable.Id_contentChange);

                    listViewHolder.cardTitle = (Text) component.findComponentById(ResourceTable.Id_cardTitle);
                    listViewHolder.cardBookName = (Text) component.findComponentById(ResourceTable.Id_cardBookName);
                    listViewHolder.cardBookbref = (Text) component.findComponentById(ResourceTable.Id_cardBookbref);
                    listViewHolder.cardFristText = (Text) component.findComponentById(ResourceTable.Id_cardFristText);
                    listViewHolder.cardTowText = (Text) component.findComponentById(ResourceTable.Id_cardTowText);
                    listViewHolder.cardThreeText = (Text) component.findComponentById(ResourceTable.Id_cardThreeText);
                    listViewHolder.cardFourText = (Text) component.findComponentById(ResourceTable.Id_cardFourText);

                    listViewHolder.cardLayout = (StackLayout) component.findComponentById(ResourceTable.Id_cardLayout);
                    listViewHolder.firstLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_firstLayout);
                    listViewHolder.towLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_towLayout);
                    listViewHolder.threeLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_threeLayout);
                    listViewHolder.fourLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_fourLayout);
                    listViewHolder.mBookInfoLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_mBookInfoLayout);

                    listViewHolder.rating = (Rating) component.findComponentById(ResourceTable.Id_rating);
                    component.setTag(listViewHolder);
                    break;
            }
        } else {
            switch (type) {
                case ITEM_TYPE_HEADER:
                    headViewHolder = (HeadViewHolder) component.getTag();
                    break;
                case ITEM_TYPE_RECOMMEND:
                    recommendViewHolder = (RecommendViewHolder) component.getTag();
                    break;
                case ITEM_TYPE_CONTENT:
                    listViewHolder = (ListViewHolder) component.getTag();
                    break;
            }
        }

        switch (type) {
            case ITEM_TYPE_HEADER:
                headViewHolder.convenientBanner.setPages(carouselImages).startTurning(3000).setCanLoop(true).start();
                headViewHolder.serch_sl.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onItemClickListener(component);
                    }
                });
                headViewHolder.dongman_cit.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onItemClickListener(component);
                    }
                });
                headViewHolder.mingxinpian_cit.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onItemClickListener(component);
                    }
                });
                headViewHolder.shujia_cit.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onItemClickListener(component);
                    }
                });
                headViewHolder.guangchang_cit.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onItemClickListener(component);
                    }
                });
                break;
            case ITEM_TYPE_RECOMMEND:
                recommendViewHolder.firstLayout.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onLayoutClickListener(component,recommendList.get(0));
                    }
                });
                recommendViewHolder.twoLayout.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onLayoutClickListener(component,recommendList.get(1));
                    }
                });
                recommendViewHolder.threeLayout.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onLayoutClickListener(component,recommendList.get(2));
                    }
                });
                if(recommendList.size() > 0){
                    Glide.with(context)
                            .load(recommendList.get(0))
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_firstImage);

                    Glide.with(context)
                            .load(recommendList.get(1))
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_twoImage);

                    Glide.with(context)
                            .load(recommendList.get(2))
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_threeImage);
                    recommendViewHolder.mp_home_recommend_fristText.setText(recommendList.get(0).getName());
                    recommendViewHolder.mp_home_recommend_twoText.setText(recommendList.get(1).getName());
                    recommendViewHolder.mp_home_recommend_threeText.setText(recommendList.get(2).getName());
                }
                break;
            case ITEM_TYPE_CONTENT:
                if(listContent.size() > 0){
                    HomeDesignBean homeDesignBean = listContent.get(mContentPosition);
                    List<SourceListContent> sourceContents = homeDesignBean.getSourceListContent();

                    if(sourceContents.size() > 0){
                        Glide.with(context)
                                .load(sourceContents.get(0).getCoverUrl())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(listViewHolder.cardTitleImage);
                        Glide.with(context)
                                .load(sourceContents.get(1).getCoverUrl())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(listViewHolder.cardFristImage);
                        Glide.with(context)
                                .load(sourceContents.get(2).getCoverUrl())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(listViewHolder.cardTowImage);
                        Glide.with(context)
                                .load(sourceContents.get(3).getCoverUrl())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(listViewHolder.cardThreeImage);
                        Glide.with(context)
                                .load(sourceContents.get(4).getCoverUrl())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(listViewHolder.cardFourImage);
                    }

                    listViewHolder.contentChange.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onContentChangeClickListener(mContentPosition, homeDesignBean.getKind());
                        }
                    });
                    listViewHolder.cardTitle.setText(homeDesignBean.getKind());
                    listViewHolder.cardBookName.setText(sourceContents.get(0).getName());
                    listViewHolder.cardBookbref.setText(sourceContents.get(0).getBookdesc());

                    listViewHolder.cardFristText.setText(sourceContents.get(1).getName());
                    listViewHolder.cardTowText.setText(sourceContents.get(2).getName());
                    listViewHolder.cardThreeText.setText(sourceContents.get(3).getName());
                    listViewHolder.cardFourText.setText(sourceContents.get(4).getName());

                    Glide.with(context)
                            .load(sourceContents.get(0).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardTitleImage);

                    Glide.with(context)
                            .load(sourceContents.get(1).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardFristImage);

                    Glide.with(context)
                            .load(sourceContents.get(2).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardTowImage);

                    Glide.with(context)
                            .load(sourceContents.get(3).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardThreeImage);

                    Glide.with(context)
                            .load(sourceContents.get(4).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardFourImage);

                    ShapeElement shapeElementBg = new ShapeElement();
                    shapeElementBg.setRgbColor(RgbColor.fromArgbInt(
                            Color.getIntColor(homeDesignBean.getCardColor())));
                    listViewHolder.mBookInfoLayout.setBackground(shapeElementBg);

                    listViewHolder.cardLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onLayoutClickListener(component, sourceContents.get(0));
                        }
                    });

                    listViewHolder.firstLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onLayoutClickListener(component, sourceContents.get(1));
                        }
                    });

                    listViewHolder.towLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onLayoutClickListener(component, sourceContents.get(2));
                        }
                    });

                    listViewHolder.threeLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onLayoutClickListener(component, sourceContents.get(3));
                        }
                    });

                    listViewHolder.fourLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onLayoutClickListener(component, sourceContents.get(4));
                        }
                    });
                }

                break;
        }

        return component;
    }

    @Override
    public int getItemComponentType(int position) {
        if (isHeaderView(position)) {
            // 头部View
            return ITEM_TYPE_HEADER;
        } else if (isRecommendView(position)) {
            return ITEM_TYPE_RECOMMEND;
        } else {
            // 内容View
            return ITEM_TYPE_CONTENT;
        }
    }

    public class HeadViewHolder {
        ConvenientBanner convenientBanner;
        StackLayout serch_sl;
        CommonImageText dongman_cit;
        CommonImageText mingxinpian_cit;
        CommonImageText shujia_cit;
        CommonImageText guangchang_cit;
    }

    public class RecommendViewHolder {
        DirectionalLayout firstLayout;
        DirectionalLayout twoLayout;
        DirectionalLayout threeLayout;
        Image mp_home_recommend_firstImage;
        Image mp_home_recommend_twoImage;
        Image mp_home_recommend_threeImage;
        Text mp_home_recommend_fristText;
        Text mp_home_recommend_twoText;
        Text mp_home_recommend_threeText;
    }

    public class ListViewHolder {
        Image cardTitleImage;
        Image cardFristImage;
        Image cardTowImage;
        Image cardThreeImage;
        Image cardFourImage;
        /**
         * 换一换图标
         */
        Image contentChange;

        Text cardTitle;
        Text cardBookName;
        Text cardBookbref;

        Text cardFristText;
        Text cardTowText;
        Text cardThreeText;
        Text cardFourText;

        StackLayout cardLayout;
        DirectionalLayout firstLayout;
        DirectionalLayout towLayout;
        DirectionalLayout threeLayout;
        DirectionalLayout fourLayout;
        DirectionalLayout mBookInfoLayout;
        /**
         * 首页Content布局中评分元素增加
         */
        Rating rating;
    }
}


