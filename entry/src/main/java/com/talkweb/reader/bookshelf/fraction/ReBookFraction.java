package com.talkweb.reader.bookshelf.fraction;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.talkweb.baselibrary.base.BaseFraction;
import com.talkweb.baselibrary.utils.LogUtil;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.bookshelf.adapter.BookSheAdapter;
import com.talkweb.reader.bookshelf.adapter.OnBookShelfAdapterClickListener;
import com.talkweb.reader.bookshelf.bean.BookBean;
import com.talkweb.reader.bookshelf.bean.BookShelfBean;
import com.talkweb.reader.maintab.bean.SearchBookBean;
import com.talkweb.reader.maintab.bean.SourceListContent;
import com.talkweb.reader.maintab.detail.BookDetailAbility;
import com.zhouyou.http.EasyHttp;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import com.zhouyou.http.model.ApiResult;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.ScaleInfo;


public class ReBookFraction extends BaseFraction {

    private ListContainer listContainer;
    private BookSheAdapter bookAdapter;

    @Override
    protected int getUIContent() {
        return ResourceTable.Layout_fraction_book_recom;
    }

    @Override
    protected void initializeComponent(Component component) {
        listContainer = (ListContainer) component.findComponentById(
                ResourceTable.Id_listContainer);
        initData();
    }

    BookShelfBean bookDatas;
    private void initData() {
        isLoading = false;
        hasMoreData = true;
        page = 1;
        LogUtil.e("data===","Book111");
        EasyHttp.get("/appview/recommendHome")
                .baseUrl("http://aimanpin.com")
                .readTimeOut(30 * 1000)//局部定义读超时
                .writeTimeOut(30 * 1000)
                .connectTimeout(30 * 1000)
                .timeStamp(true)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                    }

                    @Override
                    public void onSuccess(String response) {
                        if (response != null) {
                            Gson gson = new Gson();
                            ApiResult<BookShelfBean> bookShelfBeanApiResult = gson.fromJson(response, new TypeToken<ApiResult<BookShelfBean>>(){}.getType());
                            bookDatas = bookShelfBeanApiResult.getData();
                            LogUtil.e("BookData",bookShelfBeanApiResult.getData().toString());
                            initView();
                        }
                    }
                });
    }

    boolean isLoading;
    boolean hasMoreData = true;
    int page = 1;
    private void moreData() {
        if(!hasMoreData || isLoading){
            return;
        }
        isLoading = true;
        EasyHttp.get("/appview/recommendPagData")
                .baseUrl("http://aimanpin.com")
                .params("page", page+"")
                .readTimeOut(30 * 1000)//局部定义读超时
                .writeTimeOut(30 * 1000)
                .connectTimeout(30 * 1000)
                .timeStamp(true)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                        isLoading = false;
                    }

                    @Override
                    public void onSuccess(String response) {
                        if (response != null) {
                            page++;
                            isLoading = false;
                            Gson gson = new Gson();
                            ApiResult<BookShelfBean> bookShelfBeanApiResult = gson.fromJson(response, new TypeToken<ApiResult<BookShelfBean>>(){}.getType());
                            BookShelfBean bookDatas = bookShelfBeanApiResult.getData();
                            if(bookDatas!=null && bookDatas.getMoreRecommendList()!=null && bookDatas.getMoreRecommendList().size()>0){
                                ReBookFraction.this.bookDatas.getContentList().addAll(bookDatas.getMoreRecommendList());
                                bookAdapter.setNewData(ReBookFraction.this.bookDatas.getHotRanking(),ReBookFraction.this.bookDatas.getRecommend(), ReBookFraction.this.bookDatas.getContentList());
                            }
                        }else{
                            hasMoreData = false;
                        }
                    }
                });
    }

    private void initView(){
        if(bookDatas == null){
            return;
        }
        bookAdapter = new BookSheAdapter(getFractionAbility(), bookDatas.getHotRanking(), bookDatas.getRecommend(), bookDatas.getContentList(), new OnBookShelfAdapterClickListener() {
            @Override
            public void onItemClickListener(Component component, BookBean bookBean) {
                SearchBookBean searchBookBean = new SearchBookBean();
                searchBookBean.setName(bookBean.getName());
                searchBookBean.setCoverUrl(bookBean.getCoverUrl());
                searchBookBean.setNoteUrl(bookBean.getNoteUrl());
                searchBookBean.setAuthor(bookBean.getAuthor());
                searchBookBean.setDesc(bookBean.getBookdesc());
                searchBookBean.setOrigin(bookBean.getOrigin());
                searchBookBean.setKind(bookBean.getKind());
                searchBookBean.setTag(bookBean.getTag());
                searchBookBean.setAdd(false);
                searchBookBean.setWords(0);

                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getFractionAbility().getBundleName())
                        .withAbilityName(BookDetailAbility.class.getName())
                        .build();
                intent.setParam("searchBookBean", searchBookBean);
                intent.setOperation(operation);
                startAbility(intent);
            }

            @Override
            public void onContentChangeClickListener(int position, String kind) {

            }
        });
        listContainer.setItemProvider(bookAdapter);
        listContainer.setScrollListener(new ListContainer.ScrollListener() {
            @Override
            public void onScrollFinished() {
                int lastPosition = listContainer.getLastVisibleItemPosition();
                int size = bookAdapter.getCount();
                if(lastPosition>=size-3){//提前三个预加载
                    moreData();
                }
            }
        });
    }

}
