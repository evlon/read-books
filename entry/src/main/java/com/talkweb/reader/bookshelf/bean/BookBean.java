/**
 * Copyright 2020 bejson.com
 */
package com.talkweb.reader.bookshelf.bean;

/**
 * 书本内容JavaBean解析类
 */
public class BookBean {

    private String author;
    private String kind;
    private String origin;
    private String updateTime;
    private String lastChapter;
    private String coverUrl;
    private String searCount;
    private String schedule;
    private String noteUrl;
    private int chapterNumbe;
    private String name;
    private int id;
    private String tag;
    private String bookdesc;
    private String state;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getLastChapter() {
        return lastChapter;
    }

    public void setLastChapter(String lastChapter) {
        this.lastChapter = lastChapter;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getSearCount() {
        return searCount;
    }

    public void setSearCount(String searCount) {
        this.searCount = searCount;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getNoteUrl() {
        return noteUrl;
    }

    public void setNoteUrl(String noteUrl) {
        this.noteUrl = noteUrl;
    }

    public int getChapterNumbe() {
        return chapterNumbe;
    }

    public void setChapterNumbe(int chapterNumbe) {
        this.chapterNumbe = chapterNumbe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getBookdesc() {
        return bookdesc;
    }

    public void setBookdesc(String bookdesc) {
        this.bookdesc = bookdesc;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}