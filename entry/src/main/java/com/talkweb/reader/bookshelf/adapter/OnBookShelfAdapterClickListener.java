package com.talkweb.reader.bookshelf.adapter;


import com.talkweb.reader.bookshelf.bean.BookBean;
import com.talkweb.reader.maintab.bean.SourceListContent;
import ohos.agp.components.Component;

public interface OnBookShelfAdapterClickListener {
    void onItemClickListener(Component component, BookBean bookBean);
    void onContentChangeClickListener(int position, String kind);
}
