package com.talkweb.reader.book;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.talkweb.baselibrary.base.BaseFraction;
import com.talkweb.baselibrary.utils.PreferencesUtils;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.book.adapter.BookMySheAdapter;
import com.talkweb.reader.bookshelf.BookShelfAbility;
import com.talkweb.reader.bookshelf.adapter.OnBookShelfAdapterClickListener;
import com.talkweb.reader.bookshelf.bean.BookBean;
import com.talkweb.reader.maintab.bean.SearchBookBean;
import com.talkweb.reader.maintab.detail.BookDetailAbility;
import com.zhouyou.http.utils.TextUtils;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;

import java.util.List;


public class BookFraction extends BaseFraction {

    private Image mShopIm;
    private ListContainer listContainer;
    private Text tips_null;
    private BookMySheAdapter bookAdapter;

    @Override
    protected int getUIContent() {
        return ResourceTable.Layout_fraction_book;
    }

    @Override
    protected void initializeComponent(Component component) {
        listContainer = (ListContainer) component.findComponentById(
                ResourceTable.Id_listContainer);
        tips_null = (Text) component.findComponentById(
                ResourceTable.Id_tips_null);
        mShopIm = (Image) component.findComponentById(
                ResourceTable.Id_book_back_im);
        mShopIm.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getFractionAbility().getBundleName())
                        .withAbilityName(BookShelfAbility.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

    }

    @Override
    protected void onActive() {
        super.onActive();
        initData();
    }

    List<BookBean> bookDatas;
    private void initData() {
        String favData = PreferencesUtils.getInstance(getFractionAbility()).getString("DATA_FAV_BOOK",null);
        if(!TextUtils.isEmpty(favData)){
            tips_null.setVisibility(Component.HIDE);
            Gson gson = new Gson();
            bookDatas = gson.fromJson(favData, new TypeToken<List<BookBean>>(){}.getType());
            initView();
        }else{
            tips_null.setVisibility(Component.VISIBLE);
        }
    }

    private void initView(){
        if(bookDatas == null){
            return;
        }
        bookAdapter = new BookMySheAdapter(getFractionAbility(),bookDatas, new OnBookShelfAdapterClickListener() {
            @Override
            public void onItemClickListener(Component component, BookBean bookBean) {
                SearchBookBean searchBookBean = new SearchBookBean();
                searchBookBean.setName(bookBean.getName());
                searchBookBean.setCoverUrl(bookBean.getCoverUrl());
                searchBookBean.setNoteUrl(bookBean.getNoteUrl());
                searchBookBean.setAuthor(bookBean.getAuthor());
                searchBookBean.setDesc(bookBean.getBookdesc());
                searchBookBean.setOrigin(bookBean.getOrigin());
                searchBookBean.setKind(bookBean.getKind());
                searchBookBean.setTag(bookBean.getTag());
                searchBookBean.setAdd(false);
                searchBookBean.setWords(0);

                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getFractionAbility().getBundleName())
                        .withAbilityName(BookDetailAbility.class.getName())
                        .build();
                intent.setParam("searchBookBean", searchBookBean);
                intent.setOperation(operation);
                startAbility(intent);
            }

            @Override
            public void onContentChangeClickListener(int position, String kind) {

            }
        });
        listContainer.setItemProvider(bookAdapter);
    }

}
