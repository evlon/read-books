package com.talkweb.reader.mine;

import com.talkweb.baselibrary.base.BaseFraction;
import com.talkweb.reader.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.util.List;

public class MineFraction extends BaseFraction {

    private ListContainer listContainer;
    private List<String> list;

    @Override
    protected int getUIContent() {
        return ResourceTable.Layout_fraction_mine;
    }

    @Override
    protected void initializeComponent(Component component) {
        initData();
    }

    private void initData() {


    }

}
