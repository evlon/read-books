package com.talkweb.reader.circle.adapter;


import com.talkweb.baselibrary.banner.ConvenientBanner;
import com.talkweb.baselibrary.component.CommonImageText;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.circle.bean.ImageInfo;
import com.talkweb.reader.maintab.adapter.HomeAdapter;
import com.talkweb.reader.maintab.adapter.OnHomeAdapterClickListener;
import com.talkweb.reader.maintab.bean.HomeDesignBean;
import com.talkweb.reader.maintab.bean.SourceListContent;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public class CircleAdapter<T>  extends BaseItemProvider {

    private Context mContext;
    private List<HomeDesignBean> listContent;
    private ViewHolder viewHolder;
    private List<ImageInfo> listData;
    public CircleAdapter(Context context,List<ImageInfo> listData) {
        this.mContext = context;
        this.listData = listData;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return listData.get(i).getId();
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (component == null) {
            component = LayoutScatter.getInstance(mContext).parse(
                    ResourceTable.Layout_item_circle_list, componentContainer, false);
            viewHolder = new ViewHolder();
            viewHolder.text= (Text) component.findComponentById(ResourceTable.Id_text);
        }else {
            viewHolder = (CircleAdapter.ViewHolder) component.getTag();
        }
        if (viewHolder != null && viewHolder.text != null)
            viewHolder.text.setText("哈哈哈");
        return component;
    }
    public static  class ViewHolder {
        Text text;
    }
}
