package com.zhouyou.http.ohos;

import io.reactivex.rxjava3.disposables.Disposable;


import io.reactivex.rxjava3.openharmony.schedulers.OpenHarmonySchedulers;
import ohos.eventhandler.EventRunner;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class MainThreadDisposable implements Disposable {
    /**
     * Verify that the calling thread is the Android main thread.
     * <p>
     * Calls to this method are usually preconditions for subscription behavior which instances of
     * this class later undo. See the class documentation for an example.
     *
     * @throws IllegalStateException when called from any other thread.
     */
    public static void verifyMainThread() {
        if (EventRunner.current() != EventRunner.getMainEventRunner()) {
            throw new IllegalStateException(
                    "Expected to be called on the main thread but was " + Thread.currentThread().getName());
        }
    }

    private final AtomicBoolean unsubscribed = new AtomicBoolean();

    @Override
    public final boolean isDisposed() {
        return unsubscribed.get();
    }

    @Override
    public final void dispose() {
        if (unsubscribed.compareAndSet(false, true)) {
            if (EventRunner.current() == EventRunner.getMainEventRunner()) {
                onDispose();
            } else {
                OpenHarmonySchedulers.mainThread().scheduleDirect(() -> onDispose());
            }
        }
    }

    protected abstract void onDispose();
}
