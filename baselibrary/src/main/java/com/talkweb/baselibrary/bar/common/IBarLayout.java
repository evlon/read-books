package com.talkweb.baselibrary.bar.common;

import ohos.agp.components.ComponentContainer;

import java.util.List;

/**
 * 外层容器的接口，第一个泛型就是底部导航栏中的每个条目，第二个泛型是每个条目的数据
 */
public interface IBarLayout<Bar extends ComponentContainer, D> {

    /**
     * 根据数据查找条目
     *
     * @param info 数据
     * @return 条目
     */
    Bar findBar(D info);

    /**
     * 添加监听
     *
     * @param listener
     */
    void addBarSelectedChangeListener(OnBarSelectedListener<D> listener);

    /**
     * 默认选中的条目
     *
     * @param defaultInfo
     */
    void defaultSelected(D defaultInfo);

    /**
     * 初始化所有的条目
     *
     * @param infoList
     */
    void initInfo(List<D> infoList);

    interface OnBarSelectedListener<D> {

        /**
         * 当某个条目被选中后的回调，该方法会被调用多次
         *
         * @param index    点击后选中条目的下标
         * @param preInfo  点击前选中的条目
         * @param nextInfo 点击后选中的条目
         */
        void onBarSelectedChange(int index, D preInfo, D nextInfo);
    }
}
