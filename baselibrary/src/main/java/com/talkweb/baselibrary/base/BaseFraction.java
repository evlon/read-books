package com.talkweb.baselibrary.base;

import com.talkweb.baselibrary.dialog.LoadingDialog;
import com.talkweb.baselibrary.utils.ResourceHelper;
import com.talkweb.baselibrary.utils.Toast;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.global.resource.Resource;

public abstract class BaseFraction extends Fraction {

    private LoadingDialog loadingDialog;

    protected abstract int getUIContent();

    protected abstract void initializeComponent(Component component);

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(getUIContent(), container, false);
        initializeComponent(component);
        return component;
    }

    public void showToast(Context context, String message){
        Toast.show(context,message);
    }

    public void showToast(Context context,int messageId){
        Toast.show(context,getString(messageId));
    }

    public void showLoadingDialog(String loadingTxt){
        if(loadingDialog == null){
            loadingDialog = new LoadingDialog(this,loadingTxt);
        }
        loadingDialog.show();
    }

    public void dissmissLoadingDialog(){
        if(loadingDialog != null && loadingDialog.isShowing()){
            loadingDialog.destroy();
        }
    }

    public final String getString(int resourceId) {
        return ResourceHelper.getString(getFractionAbility(), resourceId);
    }

    public final Resource getResource(int resourceId) {
        return ResourceHelper.getResource(getFractionAbility(), resourceId);
    }

    public int getColor(int colorId) {
        try {
            return getFractionAbility().getResourceManager().getElement(colorId).getColor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    protected final void startAbility(Intent intent) {
        getFractionAbility().startAbility(intent);
    }

    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void terminateAbility() {
        super.terminateAbility();
        getFractionAbility().terminateAbility();
    }

    public final void setResult(int resultCode, Intent resultData) {
        getFractionAbility().setResult(resultCode, resultData);
    }
}
