package com.talkweb.baselibrary.component;

import com.talkweb.baselibrary.ResourceTable;
import com.talkweb.baselibrary.utils.AttrSetUtils;
import com.talkweb.baselibrary.utils.ColorUtils;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

public class TitleBar extends DependentLayout {

    private Text mLeftTitle;
    private Image mLeftImage;
    private Text mRightTitle;
    private Image mRightImage;
    private Text mTitle;
    private DirectionalLayout left_sl;
    private StackLayout right_sl;
    private TitleClickListener mTitleClickListener;
    Context mContext;

    /**
     * 自定义属性值 布局配置
     */
    static interface AttrValue {
        String TITLE = "title";
        String LEFT_TITLE = "left_title";
        String LEFT_IMAGE = "left_image";
        String TITLE_COLOR = "title_color";
        String RIGHT_TITLE = "right_title";
        String RIGHT_IMAGE = "right_image";
    }

    public TitleBar(Context context) {
        this(context, null);
    }

    public TitleBar(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public TitleBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mContext = context;
        initView(context);
        attrSet(attrSet);
    }

    private void initView(Context context) {
        Component root = LayoutScatter.getInstance(context).parse(
                ResourceTable.Layout_title_bar, this, true);
        mLeftTitle = (Text) root.findComponentById(ResourceTable.Id_left_tv);
        mRightTitle = (Text) root.findComponentById(ResourceTable.Id_right_tv);
        mTitle = (Text) root.findComponentById(ResourceTable.Id_title_tv);
        mLeftImage = (Image) root.findComponentById(ResourceTable.Id_ig_back);
        mRightImage = (Image) root.findComponentById(ResourceTable.Id_ig_right);
        left_sl = (DirectionalLayout) root.findComponentById(ResourceTable.Id_left_sl);
        right_sl = (StackLayout) root.findComponentById(ResourceTable.Id_right_sl);

        left_sl.setClickedListener(component -> {
            if (mTitleClickListener != null) {
                mTitleClickListener.leftClick(component);
            }

        });

        right_sl.setClickedListener(component -> {
            if (mTitleClickListener != null) {
                mTitleClickListener.rightClick(component);
            }
        });

    }

    private void attrSet(AttrSet attrSet) {
        String title = AttrSetUtils.getString(attrSet, AttrValue.TITLE, "");
        int title_color = AttrSetUtils.getColor(attrSet, AttrValue.TITLE_COLOR, 0);
        int leftimage = AttrSetUtils.getImageResId(attrSet, AttrValue.LEFT_IMAGE, 0);
        String leftTitle = AttrSetUtils.getString(attrSet, AttrValue.LEFT_TITLE, "");
        int rightImage = AttrSetUtils.getImageResId(attrSet, AttrValue.RIGHT_IMAGE, 0);
        String rightTitle = AttrSetUtils.getString(attrSet, AttrValue.RIGHT_TITLE, "");

        mTitle.setText(title);
        mTitle.setTextColor(new Color(title_color));

        if (leftimage > 0) {
            try {
                Resource resource = mContext.getResourceManager().getResource(leftimage);
                ImageSource imageSource = ImageSource.create(resource, null);
                PixelMap pixelmap = imageSource.createPixelmap(null);
                mLeftImage.setPixelMap(pixelmap);
            } catch (Exception ex) {

            }
        }
        mLeftTitle.setText(leftTitle);

        if (rightImage > 0) {
            try {
                Resource resource = mContext.getResourceManager().getResource(rightImage);
                ImageSource imageSource = ImageSource.create(resource, null);
                PixelMap pixelmap = imageSource.createPixelmap(null);
                mRightImage.setPixelMap(pixelmap);
            } catch (Exception ex) {

            }
        }
        mRightTitle.setText(rightTitle);
    }

    public TitleBar setLeftTitle(String leftTitle) {
        mLeftTitle.setText(leftTitle);
        return this;
    }

    public TitleBar setLeftTitle(int leftTitleId) {
        mLeftTitle.setText(leftTitleId);
        return this;
    }

    public TitleBar setRightTitle(String rightTitle) {
        right_sl.setVisibility(VISIBLE);
        if (rightTitle == null || rightTitle.length() == 0) {
            mRightTitle.setVisibility(Component.INVISIBLE);
        } else {
            mRightTitle.setText(rightTitle);
            mRightTitle.setVisibility(VISIBLE);
        }
        return this;
    }

    public TitleBar setTitleTextColor(int titleTextColor) {
        mTitle.setTextColor(new Color(titleTextColor));
        return this;
    }

    public TitleBar setRightTitleTextColor(int rightTitleTextColor) {
        mRightTitle.setTextColor(new Color(rightTitleTextColor));
        return this;
    }

    public TitleBar setRightTitleTextSize(int size) {
        mRightTitle.setTextSize(size);
        return this;
    }

    public TitleBar setRightTitle(int rightTitleId) {
        System.out.println("titleId:" + rightTitleId);
        if (rightTitleId == -1) {
            mRightTitle.setVisibility(HIDE);
        } else {
            mRightTitle.setVisibility(VISIBLE);
            mRightTitle.setText(rightTitleId);
        }
        return this;
    }

    public TitleBar setTitle(int titleId) {
        mTitle.setText(titleId);
        return this;
    }

    public TitleBar setTitle(String title) {
        mTitle.setText(title);
        return this;
    }

    public TitleBar setLeftImage(int leftImageId) {
        mLeftImage.setPixelMap(leftImageId);
        return this;
    }

    public TitleBar setRightImage(int rightImageId) {
        mRightImage.setPixelMap(rightImageId);
        mRightImage.setVisibility(VISIBLE);
        mRightTitle.setVisibility(HIDE);
        return this;
    }

    public TitleBar showTitle(boolean isShow) {
        mTitle.setVisibility(isShow ? VISIBLE : HIDE);
        return this;
    }

    public TitleBar showLeft(boolean showLeftTitle, boolean showLeftImage) {
        left_sl.setVisibility(VISIBLE);
        mLeftTitle.setVisibility(showLeftTitle ? VISIBLE : HIDE);
        mLeftImage.setVisibility(showLeftImage ? VISIBLE : HIDE);
        return this;
    }


    public TitleBar setTitleBarBackground(int resId) {
        ShapeElement shapeElement = new ShapeElement(mContext, resId);
        setBackground(shapeElement);
        return this;
    }


    public TitleBar setTitleBarBackGroundXml(int resXml) {
        ShapeElement shapeElement = new ShapeElement(mContext, resXml);
        setBackground(shapeElement);
        return this;
    }

    public TitleBar setTitleBarBackGroundRes(int resId) {
        ShapeElement shapeElementBg = new ShapeElement();
        shapeElementBg.setRgbColor(RgbColor.fromArgbInt(
                mContext.getColor(resId)));
        setBackground(shapeElementBg);
        return this;
    }

    public Text getTitle() {
        return mTitle;
    }

    public Text getLeftTitle() {
        return mLeftTitle;
    }

    public Text getRightTitle() {
        return mRightTitle;
    }

    public Image getLeftImage() {
        return mLeftImage;
    }

    public Image getRightImage() {
        return mRightImage;
    }

    /**
     * 设置titleBar背景颜色
     *
     * @param color 可为十六进制颜色值或通过context.getColor(resourceId)获取的颜色值，不能直接传入resourceId
     * @return
     */
    public TitleBar setTitleBarBackgroundColor(int color) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(ColorUtils.hex2Rgb(color));
        setBackground(shapeElement);
        return this;
    }

    public interface TitleClickListener {
        void leftClick(Component component);

        void rightClick(Component component);
    }

    public void setTitleClickListener(TitleClickListener titleClickListener) {
        mTitleClickListener = titleClickListener;
    }
}
