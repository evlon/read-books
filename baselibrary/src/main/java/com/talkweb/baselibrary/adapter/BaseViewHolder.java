package com.talkweb.baselibrary.adapter;

import ohos.agp.components.Component;
import ohos.agp.components.Text;

import java.util.HashMap;
import java.util.LinkedHashSet;

public class BaseViewHolder {

    private Component mItemView;

    private final HashMap<Integer, Component> mViews;

    private final LinkedHashSet<Integer> childClickViewIds;

    private final LinkedHashSet<Integer> itemChildLongClickViewIds;

    public BaseViewHolder(final Component view) {
        mItemView = view;
        this.mViews = new HashMap<>();
        this.childClickViewIds = new LinkedHashSet<>();
        this.itemChildLongClickViewIds = new LinkedHashSet<>();
    }

    public <T extends Component> T getView(int viewId) {
        Component view = mViews.get(viewId);
        if (view == null) {
            view = mItemView.findComponentById(viewId);
            mViews.put(viewId, view);
        }

        return (T) view;
    }

    public BaseViewHolder setText(int viewId, String value) {
        Text view = getView(viewId);
        view.setText(value);
        return this;
    }

    public BaseViewHolder setText(int viewId, int strId) {
        Text view = getView(viewId);
        view.setText(strId);
        return this;
    }

    public BaseViewHolder addOnClickListener(final int viewId) {
        childClickViewIds.add(viewId);
        final Component view = getView(viewId);
        if (view != null) {
            if (!view.isClickable()) {
                view.setClickable(true);
            }
        }

        return this;
    }
}
