package com.talkweb.baselibrary.utils;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.utils.net.Uri;

import java.util.ArrayList;

public class BundleUtils {

    public static void shareImage(Ability context, Uri imageUri) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
        intent.setAction("android.intent.action.SEND");
        intent.setParam("android.intent.extra.STREAM", imageUri);
        intent.setType("image/*");
        context.startAbility(intent);
    }

    public static void shareMultiImage(Ability context, ArrayList<Uri> imageUri) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
        intent.setAction("android.intent.action.SEND_MULTIPLE");
        intent.setSequenceableArrayListParam("android.intent.extra.STREAM", imageUri);
        intent.setType("image/*");
        context.startAbility(intent);
    }

    public static void openVideo(Ability context, Uri videoUri) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
        intent.setAction("android.intent.action.VIEW");
        intent.setUriAndType(videoUri, "video/*");
        context.startAbility(intent);

    }
}
