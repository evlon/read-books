package com.talkweb.baselibrary.utils;


import com.talkweb.baselibrary.annotation.NonNull;
import ohos.aafwk.ability.Ability;
import ohos.bundle.IBundleManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限工具类
 */

public class PermissionsUtils {

    public static final String READ_USER_STORAGE = "ohos.permission.READ_USER_STORAGE";
    public static final String WRITE_USER_STORAGE = "ohos.permission.WRITE_USER_STORAGE";
    public static final String INTERNET = "ohos.permission.INTERNET";
    //存储访问权限
    public static String[] storagePermissions = {INTERNET,READ_USER_STORAGE, WRITE_USER_STORAGE};

    private final int mRequestCode = 100;//权限请求码
    public static boolean showSystemSetting = true;

    private PermissionsUtils() {
    }

    private static PermissionsUtils permissionsUtils;
    private IPermissionsResult mPermissionsResult;

    public static PermissionsUtils getInstance() {
        if (permissionsUtils == null) {
            permissionsUtils = new PermissionsUtils();
        }
        return permissionsUtils;
    }

    public void chekPermissions(Ability context, String[] permissions, @NonNull IPermissionsResult permissionsResult) {
        mPermissionsResult = permissionsResult;


        //创建一个mPermissionList，逐个判断哪些权限未授予，未授予的权限存储到mPerrrmissionList中
        List<String> mPermissionList = new ArrayList<>();
        //逐个判断你要的权限是否已经通过
        for (int i = 0; i < permissions.length; i++) {
            if (context.verifySelfPermission(permissions[i]) != IBundleManager.PERMISSION_GRANTED) {
                mPermissionList.add(permissions[i]);//添加还未授予的权限
            }
        }

        //申请权限
        if (mPermissionList.size() > 0) {//有权限没有通过，需要申请
            context.requestPermissionsFromUser(permissions, mRequestCode);
        } else {
            //说明权限都已经通过，可以做你想做的事情去
            permissionsResult.passPermissons();
            return;
        }


    }

    //请求权限后回调的方法
    //参数： requestCode  是我们自己定义的权限请求码
    //参数： permissions  是我们请求的权限名称数组
    //参数： grantResults 是我们在弹出页面后是否允许权限的标识数组，数组的长度对应的是权限名称数组的长度，数组的数据0表示允许权限，-1表示我们点击了禁止权限

    public void onRequestPermissionsResult(Ability context, int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        boolean hasPermissionDismiss = false;//有权限没有通过
        if (mRequestCode == requestCode) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == -1) {
                    hasPermissionDismiss = true;
                }
            }
            //如果有权限没有被允许
            if (hasPermissionDismiss) {
//                if (showSystemSetting) {
//                    //跳转到系统设置权限页面，或者直接关闭页面，不让他继续访问
//
//                } else {
//                    mPermissionsResult.forbitPermissons();
//                }
                mPermissionsResult.forbitPermissons();
            } else {
                //全部权限通过，可以进行下一步操作。。。
                mPermissionsResult.passPermissons();
            }
        }

    }


    public interface IPermissionsResult {
        void passPermissons();

        void forbitPermissons();
    }


}

